package pl.edu.ug.tent.springintro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import pl.edu.ug.tent.springintro.domain.Person;

@SpringBootApplication
@ImportResource("classpath:beans.xml")
public class SpringintroApplication {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(SpringintroApplication.class, args);

		Person prezes = (Person) context.getBean("prezes");
		System.out.println(prezes);

		Person wiceprezes = (Person) context.getBean("wiceprezes");
		System.out.println(wiceprezes);

		Person sekretarka = (Person) context.getBean("sekretarka");
		System.out.println(sekretarka);

		for(int i=1; i<30; i++){
			Person person = (Person) context.getBean(String.valueOf(i));
			System.out.println(person);
		}

	}

}